﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NET_bkTarjetaCredito.Models
{
    public class tarjetaCredito
    {

        public int Id { get; set; }
        [Required]
        public string titulo { get; set; }
        [Required]
        public string numeroTarjeta { get; set; }
        [Required]
        public string fechaExpiracion { get; set; }
        [Required]
        public string cvv { get; set; }

    }
}
